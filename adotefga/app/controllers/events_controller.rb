class EventsController < InheritedResources::Base
    before_action :authenticate_user!, except: [:index]

  private

    def event_params
      params.require(:event).permit(:title, :description, :picture, :start_date, :end_date)
    end
end
